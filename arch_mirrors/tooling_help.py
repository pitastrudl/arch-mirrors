import datetime
import pathlib
from enum import Enum
from typing import Any
from collections import defaultdict

def new_defaultdict(*args, **kwargs):
	return defaultdict(new_defaultdict)


def defaultdict_to_dict(d):
	if isinstance(d, defaultdict):
		d = {k: defaultdict_to_dict(v) for k, v in d.items()}
	return d


def tomlify(obj :Any) -> Any:
	compatible_types = str, int, float, bool
	if isinstance(obj, dict):
		return {
			key: tomlify(value)
			for key, value in obj.items()
			if isinstance(key, compatible_types) and tomlify(value)
		}
	if isinstance(obj, Enum):
		return obj.value
	if hasattr(obj, 'dict'):
		return tomlify(obj.dict())
	if hasattr(obj, '__dump__'):
		return obj.__dump__()
	if isinstance(obj, (datetime.datetime, datetime.date)):
		return obj.isoformat()
	if isinstance(obj, (list, set, tuple)):
		return [tomlify(item) for item in obj]
	if isinstance(obj, pathlib.Path):
		return str(obj)
	if hasattr(obj, "__dict__"):
		return vars(obj)

	return obj