import pytest
import toml
import os

from arch_mirrors.models.versions import VersionParsers

@pytest.fixture
def tier0_mirror():
	return """
[compatability]
version = "v1"

[Tier0]
tier = 0

# In Gb/s
bandwidth = 40

https = [
	"https://archlinux.org/repo/"
]

http = [
	"http://archlinux.org/repo/"
]

rsync = [
	"rsync://archlinux.org/repo/"
]
"""

def test_valid_toml(tier0_mirror):
	data = toml.loads(tier0_mirror)

def test_valid_tier0(tier0_mirror):
	VersionParsers['maximum'].value(**toml.loads(tier0_mirror))